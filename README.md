## REKABIT

The REKA:BIT is an expansion board for the BBC Micro:BIT micro controller board to facilitate the creation of robotics projects. Since there is at present no official Arduino IDE support and I believe that a lot of people would benefit from being able to use this board in their workflow I have created this library module that you can integrate into your Arduino IDE for immediate access to all of the REKA:BIT's features.

The REKA:BIT offers

- four PWM based servo outputs via I2C
- two bi-directional DC motor outputs via I2C
- two on-board WS2812B RGB LEDs connected to PIN 8 of the Micro:BIT
- a Grove I2C pass through port (GND/3.3V/SCA/SCL)
- another five Grove ports connected to various input and output pins of the Micro:BIT

The manufacturers website is https://www.cytron.io/p-rekabit-simplifying-robotics-w-microbit 

Typical applications include all kinds of machinery, vehicles, basic robotics etcetera.

To use the BBC Micro:BIT with the Arduino eco-system and IDE you have to add board support through:

https://github.com/sandeepmistry/arduino-nRF5

## Installing

To use this library you have to add it into your Arduino IDE by [this link](https://gitlab.com/jmattheij/rekabit/-/archive/main/rekabit-main.zip) to download this repository as a zip file. This zip file you can then include in your Arduino IDE through the menu option 'Sketch/Include Library/Add .ZIP library' (the file should be in 'Downloads','Recent Files' or your home directory depending on your browser and OS). After that your sketches should be able to include the library header like so:

#include <rekabit.h>

## Getting started

In order to use this library you have to get a REKA:BIT board, a BBC Micro:BIT, some AA batteries and one or more servos or DC motors. To make it easier to integrate standard servos into your designs you can use https://www.thingiverse.com/thing:3083056 this servo adapter, for which I've made a better shaft (https://gitlab.com/jmattheij/rekabit/-/blob/main/cad/lego_servo_axle_1_.stl), you can find these in the cad subfolder of this repository as well. Under File/Examples/rekabit/rekabit_demo you'll find a simple demo program to use as a base and to test if it all works.

Feel free to use this library as the jump off point for your own unique creations, to fork this repository, hack it, add or remove stuff and to make it your own. If you do something interesting with it I'd love to hear about it via: jacques@modularcompany.com , of course any kind of feedback including bug reports is welcome. 

## Writing your own code to use the REKA:BIT

This needs to go near the top of your sketch:

  #include <rekabit.h>

In your sketch you will have to call rb_setup once in your setup code like this:

    setup()

    {
        rb_setup(false);
    }

The 'false' parameter indicates whether or not you want to use zero based addressing. If you do you'll need to set the parameter to 'true'. This gives all of your servos and motors 'C' compatible numbers with the base index at 0 rather than at 1. This allows for easier addressing of the motors when using arrays and loops.

In your loop() function you can then exercise the motors and the LEDs as shown in the rekabit_demo sketch. 

## Support
If you find a bug or have an issue then please open a ticket here. I'm pretty busy normally and don't promise any kind of response time but eventually I'll get around to it, if you have something more pressing mail me (jacques@modularcompany.com).

## Other Platforms

You *could* use the REKA:BIT with other platforms besides the BBC Micro:Bit by wiring up the I2C port provided on the Grove connector on the right hand side of the board. At a minimum you'd have to connect the GND, SDA and SCL wires to your microcontroller of choice (Aruino, Rasperry Pi, ESP32 or pretty much anything else that has an I2C bus), as well as to provide the board with (battery) power. In some cases, for instance the Arduino R4 based 'Minima' and 'UNO R4 Wifi' you'll have to add two 4.7K resistors as pullup to the 3.3V rail to SDA(A4) and SCL(A5). Maybe some enterprising soul with more PCB layout skills than I have could come up with a carrier board that would allow you to slot in an alternative micro controller into the BBC Micro:BIT compatible edge connector on the REKA:BIT. This would then give you access to the LEDs as well as the rest of the Grove connectors, and it would leave the pass-through option open.

## Examples

In the src/examples directory you'll find a simple example that puts the REKA:BIT library through its paces, if you have everything wired up properly the LEDs should blink, the servos should run through their range and the DC motors should spin in both directions.

## Roadmap

If there are new firmware revisions of the REKA:BIT I will test the software with those and if there are other boards produced with more features then I will add those. 

## Contributing
If you find a (real) bug please open an issue, or better yet, create a pull request. Don't bother opening pull requests for typos or whitespace changes. 

## Authors and acknowledgment

I would like to credit the Cytron, the makers of the REKA:BIT board with their generous support, the creator of the Lego compatible servo adapter (https://www.thingiverse.com/Daverolos), and the creator of the servo shaft adapter https://www.thingiverse.com/bpratt107. 

## License

MIT Licensed, use this code any way you want. Have Fun! And if you make a particularly neat REKA:BIT Lego project using this library I'd love to hear from you and to see your pictures.



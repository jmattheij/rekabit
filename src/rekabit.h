//
// Arduino IDE compatible module to control the REKA:BIT 
// Code by Jacques Mattheij; placed in the public domain to be used
// by anybody that sees a use for it. Bug reports welcome.

// REKA:BIT robotics controller support to be used with the BBC Micro Bit
// in combination with the Arduino IDE rather than the regular Micro:BIT tool chain.
// which allows the Micro:BIT to take advantage of the large amount of support 
// for the Arduino eco-system out there

// The REKA:BIT is a board that supports servos, motors, has two WS2812B RGB
// color LEDs on it and a number of Grove ports that can be used to further 
// expand the system.

// For more information see the manufacturer's website at:
// https://www.cytron.io/p-rekabit-simplifying-robotics-w-microbit
// and for a high resolution pinout picture see:
// https://static.cytron.io/image/catalog/products/REKABIT/REKABIT-pinout-lres.png


// read the REKA:BIT firmware version; at the time of this writing the
// firmward version is '1'

int rb_version();

// Status functions

// read the REKA:BIT battery voltage in steps of 0.1V

float rb_battery_voltage();

// determine if the power is switched on

int rb_power_on();

// determine if input voltage is too low

int rb_battery_low();


// determine if the input voltage is too high

int rb_over_voltage();

// Control functions

// Servo control functions. The REKA:BIT has four standard servo compatible
// ports each of which can drive a single servo. The brown or black servo wire should
// go to the '-' terminal, the red to the '+' (the middle) and yellow, white or orange
// to the 'S' terminal (near the long edge of the board)

// Move a servo to a particular position
// Note that:
// (1) this is a request as much as it is a command, if the servo is blocked
//     or the time required too short to make the move then it may never get
//     to the destination angle
// (2) that servos have a repeat accuracy that may not be precise enough for
//     your application unless you compensate for that
// (3) that servos consume quite a bit of power and that to 'hold' a servo in
//     place even when it isn't moving can drain your batteries pretty quickly;
//     so you'll need to release your servo if you are not using it and it is 
//     not acting against some force that would cause it to unwind.

void rb_servo(int s, int a);

// Turn off a servo, this releases the servo completely and conserves power. If there is
// a force acting on the servo shaft it may cause the servo to move away from the set position.

void rb_servo_off(int s);

// REKA:BIT DC motor control, there are two motors, MOTOR1 and MOTOR2,
// each output can power a small DC motor. Motors can be moved in either
// direction.

void rb_motor(int m, int speed);

void rb_motor_stop(int m);

// call this function once on startup to initialize the rekabit library

void rb_setup(int zerobased);

// a quick way to dump all of the relevant REKA:BIT status registers to the serial port
// this can be very useful while debugging a design.

void rb_print_status(Print * printer);



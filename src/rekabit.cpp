//
// Arduino IDE compatible module to control the REKA:BIT 
// Code by Jacques Mattheij; placed in the public domain to be used
// by anybody that sees a use for it. Bug reports welcome.

// REKA:BIT robotics controller support to be used with the BBC Micro Bit
// in combination with the Arduino IDE rather than the regular Micro:BIT tool chain.
// which allows the Micro:BIT to take advantage of the large amount of support 
// for the Arduino eco-system out there

// The REKA:BIT is a board that supports servos, motors, has two WS2812B RGB
// color LEDs on it and a number of Grove ports that can be used to further 
// expand the system.

// For more information see the manufacturer's website at:
// https://www.cytron.io/p-rekabit-simplifying-robotics-w-microbit
// and for a high resolution pinout picture see:
// https://static.cytron.io/image/catalog/products/REKABIT/REKABIT-pinout-lres.png

// I'd like to explicitly thank Cytron, the makers of the REKA:BIT for their support.

// This module uses the I2C bus protocol

#include <Wire.h>

// The I2C address that the REKA:BIT board listens to; this address is fixed so you
// can have at most one REKA:BIT board.

#define RB_I2C_ADDRESS 0x08

// Servo control slave addresses; these are the registers inside the
// Pic that powers the REKA:BIT board. The pic has 16 addressable 
// registers, some of these are write only, some are read/write and 
// some are read only. 

#define RB_REG_ADD_FW_VER    0x00 // Firmware version
#define RB_REG_ADD_SERVO_1   0x01 // Servo 1 position, default '0', 0, 50-250, 0 = Disabled, Else: value = pulse width (us) / 10
#define RB_REG_ADD_SERVO_2   0x02 // Servo 2 position, default '0', 0, 50-250, 0 = Disabled, Else: value = pulse width (us) / 10
#define RB_REG_ADD_SERVO_3   0x03 // Servo 3 position, default '0', 0, 50-250, 0 = Disabled, Else: value = pulse width (us) / 10
#define RB_REG_ADD_SERVO_4   0x04 // Servo 4 position, default '0', 0, 50-250, 0 = Disabled, Else: value = pulse width (us) / 10

// Motor control slave addresses

#define RB_REG_ADD_M1A       0x05 // Motor 1 F PWM Value, default '0', 0-255, 0 = Stop 255 = Full Speed Forward
#define RB_REG_ADD_M1B       0x06 // Motor 1 R PWM Value, default '0', 0-255, 0 = Stop 255 = Full Speed Reverse
#define RB_REG_ADD_M2A       0x07 // Motor 2 F PWM Value, default '0', 0-255, 0 = Stop 255 = Full Speed Forward
#define RB_REG_ADD_M2B       0x08 // Motor 2 R PWM Value, default '0', 0-255, 0 = Stop 255 = Full Speed Reverse

// Voltage comparator settings

#define RB_REG_ADD_LB_UTH    0x09	// Low Batt Upper Threshold, default 37, 0 - 255, Voltage * 10
#define RB_REG_ADD_LB_LTH    0x0A	// Low Batt Lower Threshold, default 36, 0 - 255, Voltage * 10
#define RB_REG_ADD_OV_TH     0x0B	// Over Voltage Threshold, default 70, 0 - 255, Voltage * 10

// Status registers

#define RB_REG_ADD_VIN       0x0C //	Vin Voltage, 0 - 255, Voltage * 10
#define RB_REG_ADD_PWR_STATE 0x0D // Power On State	-	0 - 1	0 = False, 1 = True
#define RB_REG_ADD_LB_STATE  0x0E // Low Batt State	-	0 - 1	0 = False, 1 = True
#define RB_REG_ADD_OV_STATE  0x0F // Overvoltage State	-	0 - 1	0 = False, 1 = True

// reka bit motor ids are M1, M2 for the DC motors
// and S1, S2, S3, S4 for the servos

// if you prefer zero based addressing of the motors
// then set this to 'true', and to 'false' if you want
// the '1' based addressing scheme

int rb_zero_based = false;

// write a single byte message to the i2c bus

void rb_i2c_write(int address, int r, int data)

{
  Wire.beginTransmission(address);
  Wire.write(r);
  Wire.write(data);
  int error = Wire.endTransmission();
}

// read a single byte message from the i2c bus

int rb_i2c_read(int address, int r)

{
  Wire.beginTransmission(address);
  Wire.write(r);
  Wire.endTransmission();
  Wire.requestFrom(address, 1);
  int v = Wire.read();

   return v;
}

// read the REKA:BIT firmware version; at the time of this writing the
// firmward version is '1'

int rb_version()

{
  rb_i2c_read(RB_I2C_ADDRESS, RB_REG_ADD_FW_VER);
}

// Status functions

// read the REKA:BIT battery voltage in steps of 0.1V

float rb_battery_voltage()

{
  float bv = rb_i2c_read(RB_I2C_ADDRESS, RB_REG_ADD_VIN);

  return bv / 10;
}

// determine if the power is switched on

int rb_power_on()

{
  return rb_i2c_read(RB_I2C_ADDRESS, RB_REG_ADD_PWR_STATE); 
}

// determine if input voltage is too low

int rb_battery_low()

{
  return rb_i2c_read(RB_I2C_ADDRESS, RB_REG_ADD_LB_STATE); 
}

// determine if the input voltage is too high

int rb_over_voltage()

{
  return rb_i2c_read(RB_I2C_ADDRESS, RB_REG_ADD_OV_STATE); 
}

// Control functions

// Servo control functions. The REKA:BIT has four standard servo compatible
// ports each of which can drive a single servo. The brown or black servo wire should
// go to the '-' terminal, the red to the '+' (the middle) and yellow, white or orange
// to the 'S' terminal (near the long edge of the board)

// Move a servo to a particular position
// Note that:
// (1) this is a request as much as it is a command, if the servo is blocked
//     or the time required too short to make the move then it may never get
//     to the destination angle
// (2) that servos have a repeat accuracy that may not be precise enough for
//     your application unless you compensate for that
// (3) that servos consume quite a bit of power and that to 'hold' a servo in
//     place even when it isn't moving can drain your batteries pretty quickly;
//     so you'll need to release your servo if you are not using it and it is 
//     not acting against some force that would cause it to unwind.

void rb_servo(int s, int a)

{
  if (rb_zero_based) {
    s++;
  }
  // map 1,2,3,4 to servo channels 
  switch (s) {
    case 1: 
    case 2:
    case 3:
    case 4:
      // s = s; 1:1 mapping
      break;
    default:
      return;
  }

  if (a < 0) {
    a = 0;
  }
  if (a > 180) {
    a = 180;
  }

  // valid pulse width range is 500 to 2500 micro seconds;
  // register values 50 to 250
  int pulse_width = (a * 20 / 18) + 50;

  rb_i2c_write(RB_I2C_ADDRESS, s, pulse_width);
}

// Turn off a servo, this releases the servo completely and conserves power. If there is
// a force acting on the servo shaft it may cause the servo to move away from the set position.

void rb_servo_off(int s)

{
  if (rb_zero_based) {
    s++;
  }
  if (s >= 1 && s <= 4) {
    rb_i2c_write(RB_I2C_ADDRESS, s, 0);
  }
}

// REKA:BIT DC motor control, there are two motors, MOTOR1 and MOTOR2,
// each output can power a small DC motor. Motors can be moved in either
// direction.

void rb_motor(int m, int speed)

{
  if (rb_zero_based) {
    m++;
  }
  if (speed > 255) {
    speed = 255;
  }
  if (speed < -255) {
    speed = -255;
  }

  switch (m) {
    case 1:
      if (speed >= 0) {
        rb_i2c_write(0x08, RB_REG_ADD_M1A, speed);
        rb_i2c_write(0x08, RB_REG_ADD_M1B, 0);
      }
      else {
        rb_i2c_write(0x08, RB_REG_ADD_M1A, 0);
        rb_i2c_write(0x08, RB_REG_ADD_M1B, -speed);
      }
      break;
    case 2:
     if (speed >= 0) {
        rb_i2c_write(0x08, RB_REG_ADD_M2A, speed);
        rb_i2c_write(0x08, RB_REG_ADD_M2B, 0);
      }
      else {
        rb_i2c_write(0x08, RB_REG_ADD_M2A, 0);
        rb_i2c_write(0x08, RB_REG_ADD_M2B, -speed);
      }
      break;
  }
}

void rb_motor_stop(int m)

{
  rb_motor(m, 0);
}

// call this function once on startup to initialize the library

void rb_setup(int zerobased)

{
  rb_zero_based = zerobased;

  Wire.begin(); // i2c initialization, make sure to always add this line in your 'setup' code
}

// a quick way to dump all of the relevant REKA:BIT status registers to the serial port
// this can be very useful while debugging a design.

void rb_print_status(Print * Serial)

{
  Serial->print("REKA:BIT battery voltage is ");
  Serial->print(rb_battery_voltage());
  Serial->println("V");

  Serial->print("REKA:BIT power is ");
  Serial->println(rb_power_on() ? "ON" : "OFF");

  Serial->print("REKA:BIT ");
  Serial->println(rb_battery_low() ? "Battery low" : "Battery ok");

  Serial->print("REKA:BIT ");
  Serial->println(rb_over_voltage() ? "VIN too high" : "VIN ok");
}


// A very simple demo program that puts the REKA:BIT board through its paces
// This can be used as the basis for more advanced projects. Note that useful
// information about the REKA:BIT board is dumped on the serial console which
// you can access from the Arduino IDE

#include <rekabit.h>

// The REKA:BIT board contains four servo connectors, two DC motor connectors,
// and two single wire RGB LEDs; as well as a number of pass-throughs
// using Grove connectors for i2c and GPIO pins 0,1,2,9,12,13,14,15 and 16.

// The Wire.x interface can be used to send i2c messages to other peripherals
// connected to the i2c bus. 

// The other GPIO pins function just like they would if you were to connect
// directly to the Micro:BIT. Note that GPIO 1 is shared between two of the
// Grove connectors.

// We use the Adafruit NeoPixel library in this example to talk to the LEDs.
// It would be nice to be able to use something simpler. The RGB leds are
// connected to BBC Micro:BIT GPIO pin 8.

#include <Adafruit_NeoPixel.h>

const int numPixels = 2;
const int pixelPin = 8;

Adafruit_NeoPixel ring = Adafruit_NeoPixel(numPixels, pixelPin);
uint32_t foreground = ring.Color(0x80, 0x00, 0x00); // r, g, b - blue
uint32_t background = ring.Color(0x10, 0x80, 0x10); // dim white

const int COL1 = 3;     // Column #1 control
const int LED = 25;     // 'row 1' led

void setup() {  

  rb_setup(true); // initialize the i2c bus and select 'zero based' addressing of motors and servos

  // initialize the USB serial port for easier debugging

  Serial.begin(9600);
  
  Serial.println("BBC Micro:BIT is ready!");
  Serial.print("REKA:BIT version ");
  Serial.print(rb_version());
  Serial.println("");
  
  ring.begin(); // start the NeoPixel display
}

void blink_rekaleds()

{
  // blue dot circles around a white background (for PixelRing 24)
  for (int i = 0; i < numPixels; i++) {
    ring.setPixelColor(i, foreground); // set pixel i to foreground
    ring.show();                       // actually display it
    delay(500);                         // milliseconds 
    ring.setPixelColor(i, background); // set pixel to background before moving on
    ring.show(); 
  }
}

// step the servos through their range one-by-one then turn them off again

void move_servos()

{
  for (int s=0;s<4;s++) {
   for (int i=0;i<180;i += 10) {
    rb_servo(s, i);
    delay(100);
   }

   rb_servo(s, 0);
   delay(500);
   rb_servo_off(s);
  }
}

// move both DC motors in alternating directions

void move_motors()

{
  rb_motor(0, 100);
  rb_motor(1, -100);
  delay(500);
  rb_motor(0, -100);
  rb_motor(1, 100);
  delay(500);
  rb_motor_stop(0);
  rb_motor_stop(1);
}

void loop() {
  // put your main code here, to run repeatedly:

  rb_print_status(&Serial);

  // blink the LEDs

  blink_rekaleds();

  // run the servos through a pattern

  move_servos();

  // move both dc motors to show they are alive

  move_motors();
}
